FROM python:3 as builder

ENV curl_version 7.54.1

RUN pip3 install --user speedtest-cli


FROM python:3-alpine as app

ENV PATH="${PATH}:/root/.local/bin"
RUN apk add --no-cache curl bash

COPY --from=builder /root/.local/ /root/.local/

WORKDIR /app
COPY speedtest.sh /app/speedtest.sh
COPY passwd /etc/passwd

CMD ["bash", "speedtest.sh"]
