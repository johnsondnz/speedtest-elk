#!/bin/bash
set -eo pipefail

SPEEDTEST_JSON=$(speedtest-cli --server 26747 --json)
DATEFMT=$(date +%Y.%V)

curl -u elastic:changeme -H "Content-Type: application/json" \
    -XPOST "http://$HOST:9200/speedtest-$DATEFMT/doc" \
    -d "$SPEEDTEST_JSON" >> /dev/null
